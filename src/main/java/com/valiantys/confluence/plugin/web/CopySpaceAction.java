package com.valiantys.confluence.plugin.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.valiantys.confluence.plugin.service.CopyPartialSpaceService;
import com.valiantys.confluence.plugin.utils.ICopySpaceKeys;
import org.apache.log4j.Logger;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.actions.AbstractPageAwareAction;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginState;
import com.opensymphony.util.TextUtils;

public class CopySpaceAction extends AbstractPageAwareAction {

	/**
	 * UID serial version generated.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final Logger LOG = Logger.getLogger(CopySpaceAction.class);

	/**
	 * Plugin's properties
	 */
	private Properties copySpaceProperties;

	/**
	 * true if the space's key is automaticaly generated.
	 */
	private boolean spaceKeyGenerated;

	/**
	 * Name of the new Space.
	 */
	private String newSpaceName;

	/**
	 * Name of the new Space key pattern.
	 */
	private String spaceKeyPattern;

	/**
	 * Name of the new Space key pattern.
	 */
	private String spaceKeyReplace;

	/**
	 * Name of the new Space.
	 */
	private String newSpaceKey;

	/**
	 * Name of the new Space.
	 */
	private List<String> errorMessage;

	/**
	 * Manager de space.
	 */
	private CopyPartialSpaceService particialCopySpaceManager;
	
	private boolean adHocWorkflowInstalled;
	private PluginAccessor pluginAccessor;
	private static final String AD_HOC_WORKFLOW_PLUGIN_KEY = "com.comalatech.workflow";
	private boolean checkPublishedActivated;

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String doPrepare() throws Exception {
		spaceKeyGenerated = false;
		LOG.error("spaceKeyGenerated : " + spaceKeyGenerated);
		LOG.debug("spaceKeyGenerated : " + spaceKeyGenerated);
		if (copySpaceProperties == null) {
			init();
		}
		spaceKeyGenerated = Boolean.parseBoolean(copySpaceProperties
				.getProperty(ICopySpaceKeys.SPACE_KEY_AUTO_GENERATION));
		spaceKeyPattern = copySpaceProperties.getProperty(ICopySpaceKeys.SPACE_KEY_PATTERN);
		spaceKeyReplace = copySpaceProperties.getProperty(ICopySpaceKeys.SPACE_KEY_REPLACE);
		if (spaceKeyReplace != null && spaceKeyReplace.trim().length() == 0) {
			spaceKeyReplace = null;
		}
		LOG.debug("spaceKeyPattern : " + spaceKeyPattern);
		
		adHocWorkflowInstalled = checkIfAdHocWorkflowInstalled();
		
		return SUCCESS;
	}

	private boolean checkIfAdHocWorkflowInstalled() {
		return pluginAccessor.getPlugin(AD_HOC_WORKFLOW_PLUGIN_KEY) != null
				&& pluginAccessor.getPlugin(AD_HOC_WORKFLOW_PLUGIN_KEY).getPluginState() == PluginState.ENABLED;
	}

	@Override
	public String execute() throws Exception {
		Page rootPage = (Page) getPage();
		if (copySpaceProperties == null) {
			init();
		}
		LOG.debug(" PROPERTY : " + copySpaceProperties.getProperty(ICopySpaceKeys.COPY_PROPERTY_PAGE));
		String propertyPageTitle = null;
		if (Boolean.parseBoolean(copySpaceProperties.getProperty(ICopySpaceKeys.COPY_PROPERTY_PAGE))) {
			LOG.debug(" PROPERTY : " + copySpaceProperties.getProperty(ICopySpaceKeys.COPY_PROPERTY_PAGE));
			propertyPageTitle = copySpaceProperties.getProperty(ICopySpaceKeys.PROPERTY_PAGE_TITLE);
			LOG.debug(ICopySpaceKeys.PROPERTY_PAGE_TITLE);
			LOG.debug(copySpaceProperties.getProperty(ICopySpaceKeys.PROPERTY_PAGE_TITLE));

		}
		LOG.debug("propertyPageTitle : " + propertyPageTitle);

		Space currentSpace = (Space) getSpace();
		Space newSpace = new Space();

		LOG.debug("newSpaceName : " + newSpaceName);

		LOG.debug("particialCopySpaceManager : " + particialCopySpaceManager);
		String newKey = generateSpaceKeyBySpaceName(newSpaceName, getSpaceKey());
		checkFieldsValidity(newSpaceName, newKey);
		if (errorMessage != null && errorMessage.size() > 0) {
			return INPUT;
		}
		// CCPS-4 The name of destination space must be the name of the source
		// plus the baseline name
		newSpace = particialCopySpaceManager.copySpace(getSpace(), newKey, newSpaceName, getRemoteUser());
		particialCopySpaceManager.populateSpace(currentSpace, newSpace, rootPage, propertyPageTitle, checkPublishedActivated);

		newSpaceKey = newSpace.getKey();

		LOG.debug("newSpace : " + newSpace);
		return SUCCESS;

	}

	private void checkFieldsValidity(String name, String key) {
		if (!TextUtils.stringSet(name)) {
			if (errorMessage == null) {
				errorMessage = new ArrayList<String>();
			}
			errorMessage.add(getText("space.name.not.specified"));
		}

		if (!Space.isValidGlobalSpaceKey(key)) {
			if (errorMessage == null) {
				errorMessage = new ArrayList<String>();
			}
			errorMessage.add(getText("space.custom.key.invalid"));
		}

		if (spaceManager.getSpace(key) != null) {
			if (errorMessage == null) {
				errorMessage = new ArrayList<String>();
			}
			errorMessage.add(getText("space.custom.key.exists"));

		}
	}

	private void init() {
		try {
			ClassLoader loader = CopySpaceAction.class.getClassLoader();
			LOG.debug("loader : " + loader);
			copySpaceProperties = new Properties();
			copySpaceProperties.load(loader.getResourceAsStream(ICopySpaceKeys.CONFIG_FILE));
			
		} catch (IOException e) {
			LOG.error("An error occurs during the loading of plugin property : " + e.getMessage());
			e.printStackTrace();
		}

	}

	protected String generateSpaceKeyBySpaceName(String nameSpace, String originalSpacekey) {
		LOG.debug("spaceKeyGenerated : " + spaceKeyGenerated);
		if (spaceKeyGenerated) {
			StringBuffer generatedKey = null;
			LOG.debug("spaceKeyPattern : " + spaceKeyPattern);

			if (copySpaceProperties == null) {
				init();
			}
			if (Boolean.valueOf(copySpaceProperties.getProperty(ICopySpaceKeys.SPACE_KEY_IS_PREFIXED))) {
				nameSpace = originalSpacekey + " " + nameSpace;
			}
			if (spaceKeyPattern != null) {
				nameSpace = nameSpace.toUpperCase();
				LOG.debug("nameSpace : " + nameSpace);
				generatedKey = new StringBuffer();

				if (spaceKeyReplace == null || spaceKeyReplace.trim().length() == 0) {
					Pattern p = Pattern.compile(spaceKeyPattern);
					Matcher m = p.matcher(nameSpace.toUpperCase());
					while (m.find()) {
						generatedKey.append(nameSpace.substring(m.start(), m.end()));
					}
				} else {
					int length = nameSpace.length();
					for (int i = 0; i < length; i++) {
						String s = nameSpace.substring(i, i + 1);
						if (Pattern.matches(spaceKeyPattern, s)) {
							generatedKey.append(s);
						} else {
							generatedKey.append(spaceKeyReplace);
						}
					}
				}
			}
			return generatedKey.toString();
		} else {
			return newSpaceKey;
		}

	}

	public boolean isSpaceKeyGenerated() {
		return spaceKeyGenerated;
	}

	public void setSpaceKeyGenerated(boolean spaceKeyGenerated) {
		this.spaceKeyGenerated = spaceKeyGenerated;
	}

	public String getNewSpaceName() {
		return newSpaceName;
	}

	public void setNewSpaceName(String newSpaceName) {
		this.newSpaceName = newSpaceName;
	}

	public String getNewSpaceKey() {
		return newSpaceKey;
	}

	public void setNewSpaceKey(String newSpaceKey) {
		this.newSpaceKey = newSpaceKey;
	}

	public void setSpaceManager(SpaceManager spaceManager) {
		this.spaceManager = spaceManager;
	}

	public void setParticialCopySpaceManager(CopyPartialSpaceService particialCopySpaceManager) {
		this.particialCopySpaceManager = particialCopySpaceManager;
	}

	public String getSpaceKeyPattern() {
		return spaceKeyPattern;
	}

	public void setSpaceKeyPattern(String spaceKeyPattern) {
		this.spaceKeyPattern = spaceKeyPattern;
	}

	public String getSpaceKeyReplace() {
		return spaceKeyReplace;
	}

	public void setSpaceKeyReplace(String spaceKeyReplace) {
		this.spaceKeyReplace = spaceKeyReplace;
	}

	public List<String> getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(List<String> errorMessage) {
		this.errorMessage = errorMessage;
	}

	public boolean isAdHocWorkflowInstalled() {
		return adHocWorkflowInstalled;
	}

	public void setAdHocWorkflowInstalled(boolean adHocWorkflowInstalled) {
		this.adHocWorkflowInstalled = adHocWorkflowInstalled;
	}

	public PluginAccessor getPluginAccessor() {
		return pluginAccessor;
	}

	public void setPluginAccessor(PluginAccessor pluginAccessor) {
		this.pluginAccessor = pluginAccessor;
	}

	public boolean isCheckPublishedActivated() {
		return checkPublishedActivated;
	}

	public void setCheckPublishedActivated(boolean checkPublishedActivated) {
		this.checkPublishedActivated = checkPublishedActivated;
	}
	
}
