package com.valiantys.confluence.plugin.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.atlassian.confluence.themes.StylesheetManager;
import com.atlassian.event.api.EventPublisher;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.randombits.confluence.metadata.MetadataManager;
import org.randombits.confluence.metadata.MetadataStorage;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.admin.actions.lookandfeel.DefaultDecorator;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.DefaultSaveContext;
import com.atlassian.confluence.core.PersistentDecorator;
import com.atlassian.confluence.event.events.security.ContentPermissionEvent;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Labelable;
import com.atlassian.confluence.labels.Labelling;
import com.atlassian.confluence.labels.Namespace;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.templates.PageTemplate;
import com.atlassian.confluence.pages.templates.PageTemplateManager;
import com.atlassian.confluence.search.ConfluenceIndexer;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.setup.settings.SpaceSettings;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.themes.ColourSchemeManager;
import com.atlassian.confluence.themes.ThemeManager;
import com.atlassian.confluence.themes.persistence.PersistentDecoratorDao;
import com.atlassian.confluence.util.io.IOUtils;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginState;
import com.atlassian.renderer.v2.SubRenderer;
import com.atlassian.user.User;
import com.valiantys.confluence.plugin.external.comalatech.AdHocWorkflowService;

/**
 * Manager responsible for making a complete copy of a space.
 */
public class CopyPartialSpaceServiceImpl implements CopyPartialSpaceService {

    private static final Logger log = Logger.getLogger(CopyPartialSpaceServiceImpl.class);

    private static final String BANDANA_KEY_COPYING_SPACE_KEY = "copyspace.copier.spacekey";
    private static final String AD_HOC_WORKFLOW_PLUGIN_KEY = "com.comalatech.workflow";

    private EventPublisher eventPublisher;
    private ConfluenceIndexer indexer;
    private SubRenderer subRenderer;
    private SpaceManager spaceManager;
    private PageManager pageManager;
    private ThemeManager themeManager;
    private ColourSchemeManager colourSchemeManager;
    private SettingsManager settingsManager;
    private PageTemplateManager pageTemplateManager;
    private BandanaManager bandanaManager;
    private LabelManager labelManager;
    private PersistentDecoratorDao persistentDecoratorDao;
    private CommentManager commentManager;
    private AttachmentManager attachmentManager;
    private StylesheetManager stylesheetManager;
    private AdHocWorkflowService adHocWorkflowService;
    private PluginAccessor pluginAccessor;

    private ArrayList<Page> pages;

    /*
     * (non-Javadoc)
     * 
     * @see com.valiantys.confluence.plugin.service.CopyPartialSpaceService#copySpace(com.atlassian.confluence.spaces.Space, java.lang.String,
     * java.lang.String, com.atlassian.user.User)
     */
    public Space copySpace(final Space originalSpace, final String newKey, final String newName, final User user) throws IOException {
        Space newSpace = spaceManager.getSpace(newKey);

        if (newSpace != null) {
            return null;
        }

        newSpace = spaceManager.createSpace(newKey, newName, originalSpace.getDescription().getBodyAsString(), user);
        copySpacePermissions(originalSpace, newSpace);
        newSpace.getHomePage().remove(pageManager);
        spaceManager.saveSpace(newSpace);

        copyEntityMetadata(originalSpace, newSpace);

        copyLookAndFeel(originalSpace, newSpace);
        copyDecorators(originalSpace, newSpace);
        copySpaceLabels(originalSpace, newSpace, true);
        copyPageTemplates(originalSpace, newSpace);
        copyCustomStylesheet(originalSpace, newSpace);
        spaceManager.saveSpace(newSpace);

        cleanSpacePermissions(newSpace);

        recordCopyingSpaceAgainstCopiedSpace(newSpace, originalSpace);
        return newSpace;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.valiantys.confluence.plugin.service.CopyPartialSpaceService#populateSpace(com.atlassian.confluence.spaces.Space,
     * com.atlassian.confluence.spaces.Space, com.atlassian.confluence.pages.Page, java.lang.String)
     */
    public void populateSpace(final Space originalSpace, final Space destinationSpace, final Page rootPage, final String propertyPageTitle,
            final boolean checkPublishedActivated) {
        List<PagePair> copiedPages = new ArrayList<PagePair>();

        try {
            pages = getPages(rootPage);
            copyPagesRecursive(rootPage, null, originalSpace, destinationSpace, copiedPages, checkPublishedActivated);

            destinationSpace.setHomePage(pageManager.getPage(destinationSpace.getKey(), rootPage.getTitle()));

            if (propertyPageTitle != null && propertyPageTitle.length() > 0) {
                copyPropertyPage(originalSpace, destinationSpace, copiedPages, propertyPageTitle);
            }

        } catch (IOException e) {
            e.printStackTrace();
            log.error("An error occurs during the recursive copy : " + e.getMessage());
        }
        for (Iterator<PagePair> it = copiedPages.iterator(); it.hasNext();) {
            PagePair pair = it.next();
            // Need to get this out as soon as possible before we mess with the
            // id and therefore hashcode and therefore
            // break the map.
            Page original = pair.original;
            Page copy = pair.copy;
            rebuildAncestors(copy);
            eventPublisher.publish(new ContentPermissionEvent(this, copy, null));
            indexer.reIndex(copy);
            pageManager.saveContentEntity(copy, new DefaultSaveContext(true, false, true));
            copyEntityMetadata(original, copy);

            copyScaffoldData(original, copy);

            // JIRA-171 Liste des tests : refresh
            indexer.reIndex(copy);
            renderPage(copy);
        }

        for (Iterator<PagePair> it = copiedPages.iterator(); it.hasNext();) {
            PagePair pair = it.next();
            copyEntityMetadata(pair.original, pair.copy);
            copyContentLabels(pair.original, pair.copy, true);
        }

    }

    private void copyCustomStylesheet(Space originalSpace, Space newSpace) {
        String originalSpaceStylesheet = stylesheetManager.getSpaceStylesheet(originalSpace.getKey());
        if (originalSpaceStylesheet != null && originalSpaceStylesheet.length() > 0)
        {
            stylesheetManager.addSpaceStylesheet(newSpace.getKey(), originalSpaceStylesheet);
        }
    }


    private void copyScaffoldData(final AbstractPage source, final AbstractPage destination) {
        MetadataStorage sourceData = MetadataManager.getInstance().loadWritableData(source, source.getVersion());

        Map<String, Object> sourceMap = sourceData.getBaseMap();
        MetadataStorage destinationStorage = MetadataManager.getInstance().loadWritableData(destination);

        for (String key : sourceMap.keySet()) {
            destinationStorage.setObject(key, sourceMap.get(key));
        }

        MetadataManager.getInstance().saveData(destinationStorage, true);
    }

    /**
     * Record in Bandana that <code>newSpace</code> was generated from <code>originalSpace</code>. Note that only one space can be recorded as being
     * the generator of a space, although many spaces can be generated by the same space.
     * 
     * @param newSpace the space to be marked as having been generated by a copy.
     * @param originalSpace the generating space to which the record points.
     */
    private void recordCopyingSpaceAgainstCopiedSpace(final Space newSpace, final Space originalSpace) {
        ConfluenceBandanaContext bandanaContext = new ConfluenceBandanaContext(newSpace);
        bandanaManager.setValue(bandanaContext, BANDANA_KEY_COPYING_SPACE_KEY, originalSpace.getKey());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.valiantys.confluence.plugin.service.CopyPartialSpaceService#copyDecorators(com.atlassian.confluence.spaces.Space,
     * com.atlassian.confluence.spaces.Space)
     */
    private void copyDecorators(final Space source, final Space destination) {
        List<DefaultDecorator> decorators = DefaultDecorator.getDecorators();
        for (DefaultDecorator defaultDecorator : decorators) {
            PersistentDecorator persistentDecorator = persistentDecoratorDao.get(source.getKey(), defaultDecorator.getFileName());

            if (persistentDecorator != null) {
                copyDecorator(destination, persistentDecorator, true);
            }
        }
    }

    private void copyDecorator(final Space destination, final PersistentDecorator original, final boolean isKeepMetaData) {
        String name = original.getName();
        String body = original.getBody();
        Date date = new Date();
        if (isKeepMetaData) {
            date = original.getLastModificationDate();
        }
        PersistentDecorator copy = new PersistentDecorator(destination.getKey(), name, body, date);
        persistentDecoratorDao.saveOrUpdate(copy);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.valiantys.confluence.plugin.service.CopyPartialSpaceService#setPersistentDecoratorDao(com.atlassian.confluence.themes.persistence.
     * PersistentDecoratorDao)
     */
    public void setPersistentDecoratorDao(final PersistentDecoratorDao persistentDecoratorDao) {
        this.persistentDecoratorDao = persistentDecoratorDao;
    }

    private void copyLookAndFeel(final Space originalSpace, final Space newSpace) {
        String originalThemeKey = themeManager.getSpaceThemeKey(originalSpace.getKey());
        themeManager.setSpaceTheme(newSpace.getKey(), originalThemeKey);

        SpaceSettings spaceSettings = settingsManager.getSpaceSettings(originalSpace.getKey());
        SpaceSettings newSpaceSettings = new SpaceSettings(newSpace.getKey());
        newSpaceSettings.setColourSchemesSettings(spaceSettings.getColourSchemesSettings());
        newSpaceSettings.setDisableLogo(spaceSettings.isDisableLogo());
        settingsManager.updateSpaceSettings(newSpaceSettings);

        String colourSchemeSetting = colourSchemeManager.getColourSchemeSetting(originalSpace);
        colourSchemeManager.setColourSchemeSetting(newSpace, colourSchemeSetting);
        colourSchemeManager.saveSpaceColourScheme(newSpace, colourSchemeManager.getSpaceColourSchemeIsolated(originalSpace.getKey()));
    }

    private void copySpacePermissions(final Space originalSpace, final Space newSpace) {
        boolean atLeastOneDefaultPermFound = false;

        List<SpacePermission> originalPermissions = originalSpace.getPermissions();
        for (SpacePermission originalPermission : originalPermissions) {

            // detect if at least one "confluence-users" permission exists
            String permGroupName = originalPermission.getGroup();
            if (!atLeastOneDefaultPermFound && permGroupName != null && permGroupName.equals("confluence-users"))
            {
                atLeastOneDefaultPermFound = true;
            }

            // copy VIEW, ADMINISTER, EXPORT permissions
            if (originalPermission.getType().equals(SpacePermission.VIEWSPACE_PERMISSION)
                    || originalPermission.getType().equals(SpacePermission.ADMINISTER_SPACE_PERMISSION)
                    || originalPermission.getType().equals(SpacePermission.EXPORT_PAGE_PERMISSION)) {
                SpacePermission copiedPermission = new SpacePermission();
                copiedPermission.setGroup(originalPermission.getGroup());
                copiedPermission.setSpace(newSpace);
                copiedPermission.setType(originalPermission.getType());
                copiedPermission.setUserName(originalPermission.getUserName());
                copiedPermission.isAnonymousPermission();
                newSpace.addPermission(copiedPermission);
                copyEntityMetadata(originalPermission, copiedPermission);
            }
        }

        // By default when creating a space, Confluence adds "confluence-users" permissions. If original space didn't
        // have any "confluence-users" permission, new space doesn't need to get "confluence-users" permissions
        if (!atLeastOneDefaultPermFound)
        {
            List<SpacePermission> permissionsToRemove = Lists.newArrayList();

            // prepare permissions which need to be removed
            for (SpacePermission newPerm : newSpace.getPermissions()) {
                String permGroupName = newPerm.getGroup();
                if (permGroupName != null && permGroupName.equals("confluence-users"))
                {
                    permissionsToRemove.add(newPerm);
                }
            }

            // remove these permissions
            for (SpacePermission perm : permissionsToRemove)
            {
                newSpace.removePermission(perm);
            }
        }

    }

    private void cleanSpacePermissions(final Space newSpace) {
        List<SpacePermission> permissionsToRemove = Lists.newArrayList();
        for (SpacePermission newPerm : newSpace.getPermissions()) {

            // clean NOT [VIEW, ADMINISTER, EXPORT] permissions for users/groups
            if (newPerm.isUserPermission() || newPerm.isGroupPermission())
            {
                if (!(newPerm.getType().equals(SpacePermission.VIEWSPACE_PERMISSION))
                        && !(newPerm.getType().equals(SpacePermission.ADMINISTER_SPACE_PERMISSION))
                        && !(newPerm.getType().equals(SpacePermission.EXPORT_PAGE_PERMISSION))) {
                    permissionsToRemove.add(newPerm);
                }
            }

            // remove any anonymous permission
            else if (newPerm.isAnonymousPermission())
            {
                permissionsToRemove.add(newPerm);
            }
        }

        // remove permissions
        if (permissionsToRemove != null) {
            for (SpacePermission permission : permissionsToRemove) {
                newSpace.removePermission(permission);
                log.debug("Permission removed : " + permission.getType() + " - username : [" + permission.getUserName() + "] - group : [" + permission.getGroup() + "]");
            }
        }
    }

    private void copyPagesRecursive(final Page originalPage, final Page newParent, final Space originalSpace, final Space newSpace,
            final List<PagePair> copiedPages, final boolean checkPublishedActivated) throws IOException {
        if (!adHocWorkflowPluginInstalled() || adHocWorkflowPluginInstalled()
                && (!checkPublishedActivated || checkPublishedActivated && !adHocWorkflowService.pageHasAnAssociatedWorkflow(originalPage))) {
            Page copy;
            copy = copyPage(originalPage);
            copiedPages.add(new PagePair(originalPage, copy));

            copy.setSpace(newSpace);
            if (originalPage.equals(originalPage.getSpace().getHomePage())) {
                newSpace.setHomePage(copy);
            }

            if (newParent != null) {
                newParent.addChild(copy);
            }

            copyAttachments(originalPage, copy);

            copyPageComments(originalPage, copy);

            List<Page> children = originalPage.getChildren();

            for (Iterator<Page> iterator = children.iterator(); iterator.hasNext();) {
                Page child = iterator.next();
                copyPagesRecursive(child, copy, originalSpace, newSpace, copiedPages, checkPublishedActivated);
            }
        } else if (adHocWorkflowPluginInstalled() && checkPublishedActivated && adHocWorkflowService.pageHasAnAssociatedWorkflow(originalPage)
                && adHocWorkflowService.pageHasBeanPublishedOnce(originalPage)) {

            int lastPublishedVersion = adHocWorkflowService.getPageLastPublishedVersion(originalPage);
            Page publishedVersionPage = (Page) pageManager.getPageByVersion(originalPage, lastPublishedVersion);

            Page copy;
            copy = copyPage(publishedVersionPage);
            copiedPages.add(new PagePair(originalPage, copy));

            copy.setSpace(newSpace);
            if (originalPage.equals(originalPage.getSpace().getHomePage())) {
                newSpace.setHomePage(copy);
            }

            if (newParent != null) {
                newParent.addChild(copy);
            }

            copyAttachments(originalPage, copy);

            copyPageComments(originalPage, copy);

            List<Page> children = originalPage.getChildren();

            for (Iterator<Page> iterator = children.iterator(); iterator.hasNext();) {
                Page child = iterator.next();
                copyPagesRecursive(child, copy, originalSpace, newSpace, copiedPages, checkPublishedActivated);
            }
        } else {
            log.info("Page '" + originalPage.getTitle() + " has been omitted due it's AD-HOC WORKFLOW status which isn't or hasn't been PUBLISHED");
        }
    }

    private boolean adHocWorkflowPluginInstalled() {
        return pluginAccessor.getPlugin(AD_HOC_WORKFLOW_PLUGIN_KEY) != null
                && pluginAccessor.getPlugin(AD_HOC_WORKFLOW_PLUGIN_KEY).getPluginState() == PluginState.ENABLED;
    }

    private void copyPropertyPage(final Space originalSpace, final Space newSpace, final List<PagePair> copiedPages, final String propertyPageTitle)
            throws IOException {
        Page copy;
        Page original = pageManager.getPage(originalSpace.getKey(), propertyPageTitle);
        if (original != null) {
            copy = copyPage(original);
            copy.setSpace(newSpace);
            copy.setParentPage(newSpace.getHomePage());
            // CCPS-5 do not duplicate property page
            boolean found = false;
            for (Iterator<PagePair> it = copiedPages.iterator(); it.hasNext() && !found;) {
                PagePair pp = it.next();
                found = pp.copy.getTitle().equals(copy.getTitle());
            }
            if (!found) {
                // It is added only if the property page already exist in
                // original space but not in the copied part.
                // Case the property page is in the top of the structure.
                copiedPages.add(new PagePair(original, copy));
            }
        }
    }

    private void copyPageComments(final Page from, final Page to) {
        List<Comment> originalComments = from.getComments();
        Map<Long, Comment> oldIdToCopiedComment = new HashMap<Long, Comment>();
        for (Iterator<Comment> it = originalComments.iterator(); it.hasNext();) {
            Comment oldComment = it.next();
            Comment newParent = null;
            if (oldComment.getParent() != null) {
                Long oldId = new Long(oldComment.getParent().getId());
                newParent = oldIdToCopiedComment.get(oldId);
                if (newParent != null) {
                    log.warn("Comments are out of creation date order.  Old parent id = " + oldId + ", old child id = " + oldComment.getId());
                }
            }
            Comment newComment = commentManager.addCommentToPage(to, newParent, oldComment.getBodyAsString());
            copyEntityMetadata(oldComment, newComment);
            oldIdToCopiedComment.put(new Long(oldComment.getId()), newComment);
        }
    }

    private Page copyPage(final Page original) {
        Page copy;
        copy = new Page();
        copy.setTitle(original.getTitle());
        copy.setBodyAsString(parseWysiwygLinkFormat(original.getBodyAsString(), original.getSpaceKey()));

        copy.setPosition(original.getPosition());

        // We don't copy the page restriction
        return copy;
    }

    /**
     * Call this method after creating and saving your new page to translate metedatas
     * 
     * @param newPage
     */
    private void renderPage(final Page newPage) {
        subRenderer.render(newPage.getBodyAsString(), newPage.toPageContext());
    }

    private String parseWysiwygLinkFormat(final String content, final String spaceKey) {
        String result = content;
        if (content != null && spaceKey != null) {

            String regex = null;
            for (Iterator<Page> it = pages.iterator(); it.hasNext();) {
                Page tmp = it.next();
                regex = spaceKey + ":" + tmp.getTitle();
                result = result.replaceAll(regex, tmp.getTitle());
            }

        }

        return result;
    }

    /**
     * Get all the page to delete
     * 
     * @param page The page to start
     */
    private ArrayList<Page> getPages(final Page page) {
        ArrayList<Page> res = new ArrayList<Page>();
        for (Iterator<Page> it = page.getChildren().iterator(); it.hasNext();) {
            Page p = it.next();
            res.addAll(getPages(p));
            res.add(p);
        }
        return res;
    }

    private void copyPageTemplates(final Space originalSpace, final Space newSpace) {
        List<PageTemplate> originalPageTemplates = originalSpace.getPageTemplates();
        
        for (PageTemplate originalPageTemplate : originalPageTemplates) {
        	
            PageTemplate newPageTemplate = copyPageTemplate(originalPageTemplate);
            
            newSpace.addPageTemplate(newPageTemplate);
            pageTemplateManager.savePageTemplate(newPageTemplate, null);
            
            copyPageTemplateLabels(originalPageTemplate, newPageTemplate);
            
        }
    }

	private void copyPageTemplateLabels(PageTemplate originalPageTemplate, PageTemplate newPageTemplate) {
		List<Labelling> sourceLabels = originalPageTemplate.getLabellings();
		if (sourceLabels != null && sourceLabels.size() > 0) {
		    for (Labelling aLabel : sourceLabels) {
		        labelManager.addLabel(newPageTemplate, aLabel.getLabel());
		    }
		}
	}

    private PageTemplate copyPageTemplate(final PageTemplate originalPageTemplate) {
        PageTemplate template = new PageTemplate();

        template.setName(originalPageTemplate.getName());
        template.setDescription(originalPageTemplate.getDescription());
        template.setContent(originalPageTemplate.getContent());
        
        template.setCreationDate(originalPageTemplate.getCreationDate());
        template.setCreatorName(originalPageTemplate.getCreatorName());
        template.setLastModificationDate(originalPageTemplate.getLastModificationDate());
        template.setLastModifierName(originalPageTemplate.getLastModifierName());
        
        return template;
    }

    private void copySpaceLabels(final Space originalSpace, final Space newSpace, final boolean copyPersonalLabels) {
        SpaceDescription originalSpaceDescription = originalSpace.getDescription();
        SpaceDescription newSpaceDescription = newSpace.getDescription();
        copyContentLabels(originalSpaceDescription, newSpaceDescription, copyPersonalLabels);
    }

    private void copyContentLabels(final Labelable original, final ContentEntityObject copy, final boolean copyPersonalLabels) {
        List<?> labels = original.getLabels();
        for (Iterator<?> it = labels.iterator(); it.hasNext();) {
            Label label = (Label) it.next();
            if (copyPersonalLabels || !(label.getNamespace().equals(Namespace.PERSONAL))) {
                labelManager.addLabel(copy, label);
            }
        }
    }

    private void rebuildAncestors(final Page page) {
        page.getAncestors().clear();

        if (page.getParent() != null) {
            page.getAncestors().addAll(page.getParent().getAncestors());
            page.getAncestors().add(page.getParent());
        }
    }

    protected void copyEntityMetadata(final ConfluenceEntityObject from, final ConfluenceEntityObject to) {
        to.setCreationDate(from.getCreationDate());
        to.setLastModificationDate(from.getLastModificationDate());
        to.setCreatorName(from.getCreatorName());
        to.setLastModifierName(from.getLastModifierName());
    }

    /**
     * Note: Doesn't take care of saving the page to which the attachments are added.
     */
    private void copyAttachments(final ContentEntityObject from, final ContentEntityObject to) {
        List<Attachment> attachments = attachmentManager.getLatestVersionsOfAttachments(from);
        for (Iterator<Attachment> iterator = attachments.iterator(); iterator.hasNext();) {
            Attachment attachment = iterator.next();
            Attachment attachmentCopy = new Attachment();
            attachmentCopy.setFileName(attachment.getFileName());
            attachmentCopy.setFileSize(attachment.getFileSize());
            attachmentCopy.setContentType(attachment.getContentType());
            attachmentCopy.setComment(attachment.getComment());
            attachmentCopy.setAttachmentVersion(new Integer(1));
            attachmentCopy.setContent(to);

            // Grab the InputStream from the original attachment
            InputStream data = attachmentManager.getAttachmentData(attachment);

            try {
                attachmentManager.saveAttachment(attachmentCopy, null, data);
            } catch (IOException e) {
                e.printStackTrace();
                log.error("Un error occurs during the copying of the attachements : " + e.getMessage());
            }
            copyEntityMetadata(attachment, attachmentCopy);
            IOUtils.close(data);

        }
    }

    public void setEventPublisher(final EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.valiantys.confluence.plugin.service.CopyPartialSpaceService#setIndexer(com.atlassian.confluence.search.ConfluenceIndexer)
     */
    public void setIndexer(final ConfluenceIndexer indexer) {
        this.indexer = indexer;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.valiantys.confluence.plugin.service.CopyPartialSpaceService#setSubRenderer(com.atlassian.renderer.v2.SubRenderer)
     */
    public void setSubRenderer(final SubRenderer subRenderer) {
        this.subRenderer = subRenderer;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.valiantys.confluence.plugin.service.CopyPartialSpaceService#setSpaceManager(com.atlassian.confluence.spaces.SpaceManager)
     */
    public void setSpaceManager(final SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.valiantys.confluence.plugin.service.CopyPartialSpaceService#setPageManager(com.atlassian.confluence.pages.PageManager)
     */
    public void setPageManager(final PageManager pageManager) {
        this.pageManager = pageManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.valiantys.confluence.plugin.service.CopyPartialSpaceService#setPageTemplateManager(com.atlassian.confluence.pages.templates.PageTemplateManager)
     */
    public void setPageTemplateManager(final PageTemplateManager pageTemplateManager) {
        this.pageTemplateManager = pageTemplateManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.valiantys.confluence.plugin.service.CopyPartialSpaceService#setCommentManager(com.atlassian.confluence.pages.CommentManager)
     */
    public void setCommentManager(final CommentManager commentManager) {
        this.commentManager = commentManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.valiantys.confluence.plugin.service.CopyPartialSpaceService#setBandanaManager(com.atlassian.bandana.BandanaManager)
     */
    public void setBandanaManager(final BandanaManager bandanaManager) {
        this.bandanaManager = bandanaManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.valiantys.confluence.plugin.service.CopyPartialSpaceService#setAttachmentManager(com.atlassian.confluence.pages.AttachmentManager)
     */
    public void setAttachmentManager(final AttachmentManager attachmentManager) {
        this.attachmentManager = attachmentManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.valiantys.confluence.plugin.service.CopyPartialSpaceService#setLabelManager(com.atlassian.confluence.labels.LabelManager)
     */
    public void setLabelManager(final LabelManager labelManager) {
        this.labelManager = labelManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.valiantys.confluence.plugin.service.CopyPartialSpaceService#setThemeManager(com.atlassian.confluence.themes.ThemeManager)
     */
    public void setThemeManager(final ThemeManager themeManager) {
        this.themeManager = themeManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.valiantys.confluence.plugin.service.CopyPartialSpaceService#setColourSchemeManager(com.atlassian.confluence.themes.ColourSchemeManager)
     */
    public void setColourSchemeManager(final ColourSchemeManager colourSchemeManager) {
        this.colourSchemeManager = colourSchemeManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.valiantys.confluence.plugin.service.CopyPartialSpaceService#setSettingsManager(com.atlassian.confluence.setup.settings.SettingsManager)
     */
    public void setSettingsManager(final SettingsManager settingsManager) {
        this.settingsManager = settingsManager;
    }

    public void setAdHocWorkflowService(final AdHocWorkflowService adHocWorkflowService) {
        this.adHocWorkflowService = adHocWorkflowService;
    }

    public void setPluginAccessor(final PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    public void setStylesheetManager(StylesheetManager stylesheetManager) {
        this.stylesheetManager = stylesheetManager;
    }

    private static class PagePair {
        public final Page original;
        public final Page copy;

        public PagePair(final Page oldPage, final Page newPage) {
            this.original = oldPage;
            this.copy = newPage;
        }
    }
}
