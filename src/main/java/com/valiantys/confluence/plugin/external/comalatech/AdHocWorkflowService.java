package com.valiantys.confluence.plugin.external.comalatech;

import com.atlassian.confluence.pages.Page;

public interface AdHocWorkflowService {

    public boolean pageHasAnAssociatedWorkflow(Page originalPage);

    public boolean pageHasBeanPublishedOnce(Page originalPage);

    public int getPageLastPublishedVersion(Page originalPage);

}
