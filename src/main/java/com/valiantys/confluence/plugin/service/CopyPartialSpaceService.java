package com.valiantys.confluence.plugin.service;

import java.io.IOException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.user.User;

public interface CopyPartialSpaceService {

	/**
	 * Make a deep copy of a space, including the pages within the space.
	 * 
	 * @param originalSpace the space to be copied
	 * @param newKey key to give the new space
	 * @param newName the name to give the new space.
	 * @param user the user to create objects as. Where possible the original
	 *            users will be used if specified in <code>options</code>
	 * @return the newly created space.
	 * @throws IOException if a failure to read or write occurs
	 */
	public abstract Space copySpace(Space originalSpace, String newKey,
			String newName, User user) throws IOException;

	public abstract void populateSpace(Space originalSpace,
			Space destinationSpace, Page rootPage, String propertyPageTitle, boolean checkPublishedActivated);

}