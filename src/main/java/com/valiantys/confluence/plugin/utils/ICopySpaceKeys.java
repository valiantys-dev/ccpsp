package com.valiantys.confluence.plugin.utils;


public class ICopySpaceKeys {

	public static final String CONFIG_FILE = "copyspace.properties";

    public static final String PREFIX_INDEX = "prefix.index.page";

    public static final String ENTETE_COL_NOM_PAGE = "entete.col.nom.page";
	
	public static final String ENTETE_COL_VERSION = "entete.col.version";
	
	public static final String BASE_LINE_LABEL = "baseline.label";
	
	public static final String COMMENT_VERSION = "comment.version";

    public static final String SPACE_KEY_AUTO_GENERATION = "copy.space.automatic.key.generation";
	
	public static final String SPACE_KEY_IS_PREFIXED = "copy.space.key.prefixed.by.original.space";
	
	public static final String SPACE_KEY_PATTERN = "copy.space.key.pattern";
	
	public static final String SPACE_KEY_REPLACE = "copy.space.key.replace";
	
	public static final String PROPERTY_PAGE_TITLE = "helper.property.page.default";
	
	public static final String DOCUMENT_PAGE_SUFFIX = "helper.document.page.suffix";
	
	public static final String COPY_PROPERTY_PAGE = "helper.property.page.to.copy";
}
