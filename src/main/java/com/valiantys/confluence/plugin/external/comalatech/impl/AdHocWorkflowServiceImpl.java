package com.valiantys.confluence.plugin.external.comalatech.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Page;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginState;
import com.valiantys.confluence.plugin.external.comalatech.AdHocWorkflowService;

public class AdHocWorkflowServiceImpl implements AdHocWorkflowService {

    private final static Logger LOG = Logger.getLogger(AdHocWorkflowServiceImpl.class);

    private static final String AD_HOC_WORKFLOW_PLUGIN_KEY = "com.comalatech.workflow";
    private PluginAccessor pluginAccessor;

    public void setPluginAccessor(final PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    private Object getWorkflowService() {
        Plugin aPlugin = pluginAccessor.getPlugin(AD_HOC_WORKFLOW_PLUGIN_KEY);
        return fetchPluginModuleByInterfaceName(aPlugin, "com.comalatech.workflow.WorkflowService");
    }

    private Object getStateService() {
        Plugin aPlugin = pluginAccessor.getPlugin(AD_HOC_WORKFLOW_PLUGIN_KEY);
        return fetchPluginModuleByInterfaceName(aPlugin, "com.comalatech.workflow.StateService");
    }

    public boolean pageHasAnAssociatedWorkflow(final Page aPage) {
        Object workflowService = getWorkflowService();

        Method getActiveWorkflowsMethod;
        try {
            getActiveWorkflowsMethod = workflowService.getClass().getMethod("getActiveWorkflows", ContentEntityObject.class);
            List workflows = (List) getActiveWorkflowsMethod.invoke(workflowService, aPage);
            return workflows != null && workflows.size() > 0;
        } catch (SecurityException e) {
            LOG.error(e);
        } catch (NoSuchMethodException e) {
            LOG.error(e);
        } catch (IllegalArgumentException e) {
            LOG.error(e);
        } catch (IllegalAccessException e) {
            LOG.error(e);
        } catch (InvocationTargetException e) {
            LOG.error(e);
        }

        return false;
    }

    public boolean pageHasBeanPublishedOnce(final Page aPage) {
        Object stateService = getStateService();

        try {
            Method getPublishedStateMethod = stateService.getClass().getMethod("getPublishedState", ContentEntityObject.class);
            Object state = getPublishedStateMethod.invoke(stateService, aPage);

            boolean pageHasBeanPublishedOnce = state != null;
            return pageHasAnAssociatedWorkflow(aPage) && pageHasBeanPublishedOnce;
        } catch (SecurityException e) {
            LOG.error(e);
        } catch (NoSuchMethodException e) {
            LOG.error(e);
        } catch (IllegalArgumentException e) {
            LOG.error(e);
        } catch (IllegalAccessException e) {
            LOG.error(e);
        } catch (InvocationTargetException e) {
            LOG.error(e);
        }

        return false;
    }

    public int getPageLastPublishedVersion(final Page aPage) {
        Object stateService = getStateService();

        try {
            Method getPublishedStateMethod = stateService.getClass().getMethod("getPublishedState", ContentEntityObject.class);
            Object state = getPublishedStateMethod.invoke(stateService, aPage);

            Method getContentVersionMethod = state.getClass().getMethod("getContentVersion");
            return (Integer) getContentVersionMethod.invoke(state);

        } catch (SecurityException e) {
            LOG.error(e);
        } catch (NoSuchMethodException e) {
            LOG.error(e);
        } catch (IllegalArgumentException e) {
            LOG.error(e);
        } catch (IllegalAccessException e) {
            LOG.error(e);
        } catch (InvocationTargetException e) {
            LOG.error(e);
        }

        return -1;
    }

    private Object fetchPluginModuleByInterfaceName(final Plugin aPlugin, final String aPluginModuleInterface) {
        if (aPlugin != null && aPlugin.getPluginState() == PluginState.ENABLED) {
            Collection<ModuleDescriptor<?>> moduleDescriptors = aPlugin.getModuleDescriptors();
            for (ModuleDescriptor<?> aDescriptor : moduleDescriptors) {
                if (aDescriptor != null && aDescriptor.getModuleClass() != null) {
                    for (Class aInterface : aDescriptor.getModuleClass().getInterfaces()) {
                        if (aInterface != null && aInterface.getName() != null) {
                            if (aInterface.getName().equals(aPluginModuleInterface)) {
                                return aDescriptor.getModule();
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
}
